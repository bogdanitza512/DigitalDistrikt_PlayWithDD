﻿using System.Collections;
using System.Collections.Generic;
using UniExt;
using UnityEngine;
using UnityEngine.EventSystems;

/// <summary>
/// 
/// </summary>
public class ParallaxMask : MonoBehaviour
{

    #region Nested Types

    [System.Serializable]
    struct ParallaxLayer
    {
        public Transform root;

        public float offset;

        public Vector3 startPos;

        public void initLayer()
        {
            startPos = root.position;
        }

        public void UpdatePosition(Vector2 parallaxDelta)
        {
            print("Paralax Delta: {0}".FormatWith(parallaxDelta));

            var newPos = new Vector3(startPos.x + (parallaxDelta.x * offset),
                                     startPos.y + (parallaxDelta.y * offset),
                                     startPos.z);
           
            root.position = newPos;


        }
    }

    #endregion

    #region Fields and Properties

    [SerializeField]
    public Canvas parentCanvas;

    [SerializeField]
    float mouseSensitivity;

    [SerializeField]
    List<ParallaxLayer> layers;

    #endregion

    #region Unity Messages

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    void Start()
    {
        foreach (var layer in layers)
        {
            layer.initLayer();
        }
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabled.
    /// </summary>
    void Update()
    {
        Vector2 movePos;

        RectTransformUtility.ScreenPointToLocalPointInRectangle(parentCanvas.transform as RectTransform,
                                                                Input.mousePosition,
                                                                parentCanvas.worldCamera,
                                                                out movePos);
        //transform.position = transform.TransformPoint(pos);


        //var mousePos = Camera.main.ScreenToViewportPoint(Input.mousePosition);


        foreach (var layer in layers)
        {
            layer.UpdatePosition(parentCanvas.transform.TransformPoint(movePos));
        }
    }

    #endregion

    #region Methods

    private void ExecuteParallaxMovement()
    {

    }

    #endregion

}